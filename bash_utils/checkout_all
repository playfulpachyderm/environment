#!/bin/bash

# checkout develop of all repos


if [[ -z $1 ]] 
then
	set -- "develop"
fi

case $2 in 
"b2c")
	echo "Executing checkout_all on B2C"
	cd $b2c_src
	;;
"b2b")
	echo "Executing checkout_all on B2B"
	cd $b2b_src
	;;
"")
	color yellow "No environment provided, assuming B2B."
	echo "Executing checkout_all on B2B"
	cd $b2b_src
	;;
*)
	color red "Invalid environment: $1, $2"
	exit 1
	;;
esac

echo "Checking out $1 for all repos where it exists, defaulting to develop where it doesn't"

SOURCE_REPOS=$(find * -maxdepth 0 -type d)
failures=""
successes=""

for repo in $SOURCE_REPOS
do
	checkout_branch=$1
	cd $repo
	echo
	echo "--------------------------"
	echo "Updating repo: $repo"
	echo

	current_branch=$(git symbolic-ref --short HEAD)
	fail=0

	echo "Checking if $checkout_branch exists"
	color yellow
	git fetch -q origin $checkout_branch
	if [[ $? != 0 ]]
	then
		color yellow "No such branch ($checkout_branch) in this repo.  Using develop instead!"
		checkout_branch="develop"		
	fi

	if [[ $current_branch = $checkout_branch ]]
	then
		if [[ $checkout_branch == $1 ]]
		then
			color green "$repo is already on $checkout_branch"
		else
			echo "$repo is already on $checkout_branch"
		fi
	else
		color yellow "On branch $current_branch.  Checking out $checkout_branch."
		echo "Executing: git checkout $checkout_branch"
		git checkout $checkout_branch

		if [[ $? != 0 ]]
		then
			color red "Checkout failed! (repo: $repo)"
			fail=1
		fi
	fi

	echo "Executing: git pull"
	git pull
	if [[ $? != 0 ]]
	then
		color red "pull failed! (repo: $repo)"
		fail=1
	fi

	git branchprune

	if [[ $fail == 1 ]]
	then
		failures="$repo $failures"
	else
		if [[ $checkout_branch == $1 ]]
		then
			color green "Checked out $checkout_branch for $repo."
			successes="$repo $successes"
		else
			color yellow "Checked out $checkout_branch for $repo."
		fi
	fi
	cd ..
done

echo

if [[ -n $failures ]]
then
	color red "checkout_all did not work.  Failures in the following repos:"
	color red "$failures"
else
	color green "checkout_all worked.  Checked out branches in following repos:"
	color green "$successes"
fi

show_branches
