function rot13(a) {
	var translator = new Map();
	["a".charCodeAt(0), "A".charCodeAt(0)].map(function(i) {
		for (var j = 0; j < 26; ++j) {
			translator.set(String.fromCharCode(i + j), String.fromCharCode(i + ((j + 13) % 26)));
		}
	});

	var b = "";
	for (var i = 0; i < a.length; ++i) {
		var c = a.charAt(i);
		b += translator.get(c) || c;
	}
	return b;
}

function rot13_node(node) {
	console.log("Applying rot13 to node: " + node);
	if (node == undefined)
		return;

	var tags = node.getElementsByTagName("*");

	console.log(tags.length + " tags!");

	/*
		$(tags[i]).contents().filter(function() {
			return this.nodeType == Node.TEXT_NODE;
		}).each(function(_,tag) {
			console.log(tag);
			tag.textContent=rot13(tag.textContent);
		});
	*/

	/* non-jquery reliant version of the same thing: */
	for (var i = 0; i < tags.length; ++i) {
		tagName = tags[i].tagName.toLowerCase();
		if (tagName == "style" || tagName == "script") {
			console.log("Skipping " + tagName + " tag");
			continue;
		}

		for (var j = 0; j < tags[i].childNodes.length; ++j) {
			var childNode = tags[i].childNodes[j];
			if (childNode.nodeType == Node.TEXT_NODE) {
				childNode.textContent = rot13(childNode.textContent);
			}
		}
	}
}

(function(){rot13_node(document.body);})();
