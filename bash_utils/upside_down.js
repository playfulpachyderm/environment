translate = {
	"a": "ɐ", 
	"b": "q",
	"c": "ɔ", 
	"d": "p",
	"e": "ǝ", 
	"f": "ɟ", 
	"g": "ƃ", 
	"h": "ɥ", 
	"i": "ᴉ", 
	"j": "ɾ", 
	"k": "ʞ", 
	"m": "ɯ", 
	"n": "",
	"p": "d",
	"q": "b",
	"r": "ɹ", 
	"t": "ʇ", 
	"": "n",
	"v": "ʌ", 
	"w": "ʍ", 
	"y": "ʎ", 
	"A": "∀", 
	"B": "𐐒", 
	"C": "Ɔ", 
	"D": "◖", 
	"E": "Ǝ", 
	"F": "Ⅎ", 
	"G": "פ", 
	"J": "ſ", 
	"K": "⋊", 
	"L": "⅂", 
	"M": "W",
	"N": "ᴎ", 
	"P": "Ԁ", 
	"Q": "Ό", 
	"R": "ᴚ", 
	"T": "⊥", 
	"": "∩", 
	"V": "ᴧ", 
	"W": "M",
	"Y": "⅄", 
	"3": "Ɛ", 
	"4": "ᔭ", 
	"6": "9",
	"7": "Ɫ", 
	"9": "6",
	"(": ")",
	")": "(",
	"[": "]",
	"]": "[",
	"{": "}",
	"}": "{",
	"<": ">",
	">": "<",
	".": "˙", 
	",": "'",
	"'": ",",
	"\"": "„", 
	"?": "¿", 
	"!": "¡", 
	"&": "⅋", 
	";": "؛", 
	"_": "‾", 
}

for (prop in translate) {
	translate[translate[prop]] = prop;
}

function upside_down(s) {
	s_reverse = s.split("").reverse();
	return s_reverse.map(function(i){ return translate[i] || i; }).join("");
}


function upside_down_node(node) {
	console.log("Applying upside_down to node: " + node);
	if (node == undefined)
		return;

	var tags = node.getElementsByTagName("*");

	console.log(tags.length + " tags!");

	/*
		$(tags[i]).contents().filter(function() {
			return this.nodeType == Node.TEXT_NODE;
		}).each(function(_,tag) {
			console.log(tag);
			tag.textContent=upside_down(tag.textContent);
		});
	*/

	/* non-jquery reliant version of the same thing: */
	for (var i = 0; i < tags.length; ++i) {
		tagName = tags[i].tagName.toLowerCase();
		if (tagName == "style" || tagName == "script") {
			console.log("Skipping " + tagName + " tag");
			continue;
		}

		for (var j = 0; j < tags[i].childNodes.length; ++j) {
			var childNode = tags[i].childNodes[j];
			if (childNode.nodeType == Node.TEXT_NODE) {
				childNode.textContent = upside_down(childNode.textContent);
			}
		}
	}
}

(function(){upside_down_node(document.body);})();
