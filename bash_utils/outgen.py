# out[a-z]* de[a-z]*[oe]r

from random import choice as c

def az(n):
	return "".join([c("abcdefghijklmnopqrstuvwxyz") for i in range(n)])

def out():
	return "out{} de{}{}r".format(az(c(range(5,8))), az(c(range(5,10))), c("oe"))

if __name__ == "__main__":
	import sys

	try:
		n = int(sys.argv[1])
	except:
		n = 1

	for i in range(n):
		print(out())
