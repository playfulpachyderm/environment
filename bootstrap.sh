apt-get update

# == git ==
apt-get install git -y
git config --global user.name "Alessio"
git config --global user.email "playful.pachyderm@gmail.com"

chmod 600 /home/vagrant/.ssh/id_rsa

# == misc ==
export EDITOR="$VISUAL"

echo "export VISUAL=vim" >> /home/vagrant/.bashrc
echo 'export EDITOR="$VISUAL"' >> /home/vagrant/.bashrc

apt-get install vim -y
apt-get install curl -y
apt-get install tree -y
apt-get install dos2unix -y

echo 'PATH="/home/vagrant/bash_utils:$PATH"' >> /home/vagrant/.bashrc
echo 'export PYTHONPATH="/home/vagrant/cryptomath:$PYTHONPATH"' >> /home/vagrant/.bashrc

# == Python ==
apt-get install python3-pip python3-dev -y
echo "alias python=python3" >> /home/vagrant/.bashrc
echo "alias pip=pip3" >> /home/vagrant/.bashrc

# == beautifulsoup ==
pip3 install beautifulsoup4

# == fuck ==
pip3 install thefuck -q
echo 'eval "$(thefuck --alias)"' >> /home/vagrant/.bashrc

# == ruby ==
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
if [[ $? -ne 0 ]]; then
	command curl -sSL https://rvm.io/mpapis.asc | gpg --import -
fi

# if this line is breaking, check back here: https://github.com/rvm/rvm/issues/4136
\curl -sSL https://raw.githubusercontent.com/wayneeseguin/rvm/stable/binscripts/rvm-installer | sudo bash -s stable --ruby

source /usr/local/rvm/scripts/rvm
