# To create a new project with pre-setup environment

- `git clone git@gitlab.com:playfulpachyderm/environment.git [project name]`
- create new repository on gitlab
- cd to new cloned folder
- `git remote rename origin environment`
- `git remote add origin [url for new gitlab repo]`
- modify `Vagrantfile` and `bootstrap.sh` if necessary
	- remove rails install if not needed
	- if on mac / linux, change line in Vagrantfile that copies the ssh key (currently looks in `C:/Users/User`)
- `vagrant up`
- replace this README (this will be done automatically if you create a Rails project)

This will create a new empty project with a nicely configured Vagrant machine.

The vagrant machine is bootstrapped with some software which (for some reason) doesn't come with the machine image:
- git
- vim
- curl
- tree
- dos2unix
- fuck
- python 3 with pip 3 (re-aliased as "python" and "pip" respectively, because who uses python 2)
- ruby
- Ruby on Rails

It also configures git a bit, including a .gitconfig file.
Finally, there are some "bash utils" (mostly toys).

This bootstrap does not actually create a Rails project.


## TODO

- maybe it should create a rails project (or just include an empty project?)
- creating a new rails project creates a conflict with the `.gitignore` from this repo
- creating a new rails project fails on the `bundle` step, you have to run `rvmsudo bundle` to work around it
- kind of ugly to have the actual project live in `/vagrant` in the guest machine, plus the `.gitignore` conflict thing
- possibly better way of syncing the `.gitconfig`?
